import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './Fighter'

class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    try {
      fighterService.getFighterDetails(fighter._id)
          .then(
              result => {
                  //If the fighter isn't in the Map, we create and set him
                  if (!this.fightersDetailsMap.has(fighter._id)) {
                      this.fightersDetailsMap.set(fighter._id, new Fighter(result));
                  }
                  return this.fightersDetailsMap.get(fighter._id);
              },
              error => {
                  throw error;
              }
          )
          .then (
              result => {
                  //Show fighter attributes from the Map
                  document.getElementById('name').innerText = `${result.fighterAtrebuts.get('name')}`;
                  document.getElementById('health').innerText = `Health: ${result.fighterAtrebuts.get('health')}`;
                  document.getElementById('attack').innerText = `Attack: ${result.fighterAtrebuts.get('attack')}`;
                  document.getElementById('defense').innerText = `Defense: ${result.fighterAtrebuts.get('defense')}`;

                  const infoWindow = document.getElementById('fighterInfo');
                  infoWindow.style.visibility = "visible";

                  //If a user click on the form-block we do nothing
                  //else we hide the form-block
                  let isForm = false;
                  document.getElementById('infoForm').addEventListener('click', () => {
                      isForm = true, setTimeout(() => {isForm = false}, 200)
                  }, true);
                  infoWindow.addEventListener('click', () => {
                      if (!isForm) infoWindow.style.visibility = "hidden"
                  }, false);
              },
              error => {
                  throw error;
              }
          );
    }
    catch (error) {
        throw error;
    }
  }

}

export default FightersView;