

class Fighter {
    fighterAtrebuts = new Map();
    constructor(fighter) {
        this.fighterAtrebuts.set("name", fighter.name);
        this.fighterAtrebuts.set("health", fighter.health);
        this.fighterAtrebuts.set("attack", fighter.attack);
        this.fighterAtrebuts.set("defense", fighter.defense);
    }
}

export default Fighter;